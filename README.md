# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

## Submitter: Jonathan Fujii, jfujii@uoregon.edu ##

### Grading Rubric ###

* Your code works as expected: 100 points

* For every wrong functionality (i.e., (a), (b), and (c) above), 20 points will be docked off. 

* If none of the functionalities work, 40 points will be given. Assuming the credentials.ini is submitted with the correct URL of your repo.

* If credentials.ini is missing, 0 will be assigned.
